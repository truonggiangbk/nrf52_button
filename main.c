/**
 * Copyright (c) 2014 - 2020, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup blinky_example_main main.c
 * @{
 * @ingroup blinky_example
 * @brief Blinky Example Application main file.
 *
 * This file contains the source code for a sample application to blink LEDs.
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "boards.h"
#include "nrf52_gpio.h"
#include "app_timer.h"
#include "nrf_drv_clock.h"
/**
 * @brief Function for application main entry.
 */
 
APP_TIMER_DEF(m_blinky_timer_id);  
APP_TIMER_DEF(m_toggle_timer_id);  
 
static void lfclk_request(void)
{
    ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);
    nrf_drv_clock_lfclk_request(NULL);
} 
 
/**@brief Timeout handler for the repeated timer.
 */
static void repeated_timer_handler(void * p_context)
{
//    nrf52_output_toggle(17);
}

static void toggle_timer_handler(void * p_context)
{
//		nrf52_output_toggle(18);
}


static void create_timers()
{
    ret_code_t err_code;

    // Create timers
    err_code = app_timer_create(&m_blinky_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                repeated_timer_handler);
    APP_ERROR_CHECK(err_code);
	
    // Create timers
    err_code = app_timer_create(&m_toggle_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                toggle_timer_handler);
    APP_ERROR_CHECK(err_code);	
}

extern uint32_t button_press_s;
extern uint32_t button_press_count;
extern bool button_press_flag;

int main(void)
{
    nrf52_output_create(17);
    nrf52_output_create(18);
    nrf52_output_set_level(17, 0);
    nrf52_output_set_level(18, 0);	
	
		nrf52_button_timer_init();
		nrf52_button_create(13);
	
		lfclk_request();
		app_timer_init();
		create_timers();
		ret_code_t err_code = app_timer_start(m_blinky_timer_id, APP_TIMER_TICKS(100), NULL);
		err_code = app_timer_start(m_toggle_timer_id, APP_TIMER_TICKS(500), NULL);
		APP_ERROR_CHECK(err_code);

    /* Toggle LEDs. */
    while (true)
    {
			if(button_press_flag == true)
			{
				if((button_press_s < 1) && (button_press_count == 1)){		// short press
					nrf52_output_toggle(17);
				}
				else if(button_press_count == 3)
				{
					nrf52_output_toggle(18);
				}
				else if((button_press_s > 3) && (button_press_count == 1))
				{
					nrf52_output_toggle(17);
					nrf52_output_toggle(18);
				}
				button_press_flag = false;
			}
    }
}

/**
 *@}
 **/
